-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 11 Gru 2019, 00:29
-- Wersja serwera: 10.4.8-MariaDB
-- Wersja PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `kozjan`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawa`
--

CREATE TABLE `dostawa` (
  `id_dostawa` int(10) UNSIGNED NOT NULL,
  `koszt_dostawy` float NOT NULL,
  `rodzaj_dostawy` varchar(50) NOT NULL,
  `dostawca` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `dostawa`
--

INSERT INTO `dostawa` (`id_dostawa`, `koszt_dostawy`, `rodzaj_dostawy`, `dostawca`) VALUES
(1, 11, 'InPost Paczkomaty 24/7, przelew', 'InPost'),
(2, 14, 'InPost Paczkomaty 24/7, pobranie ', 'InPost'),
(3, 12, 'Kurier DPD, przelew', 'DPD'),
(4, 15, 'Kurier DPD, pobranie', 'DPD'),
(5, 14, 'Paczka48 / Poczta Polska, przelew', 'Poczta Polska'),
(6, 18, 'Paczka48 / Poczta Polska, pobranie', 'Poczta Polska'),
(7, 0, 'Odbiór osobisty w Raciborzu', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `faktury`
--

CREATE TABLE `faktury` (
  `id_faktury` int(10) UNSIGNED NOT NULL,
  `nr_faktury` int(10) UNSIGNED NOT NULL,
  `data_sprzedazy` datetime NOT NULL,
  `wartosc_brutto` float NOT NULL,
  `wartosc_netto` float NOT NULL,
  `stawka_vat` float NOT NULL,
  `forma_platnosci` varchar(255) NOT NULL,
  `rodzaj_dokumentu` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gwarancje`
--

CREATE TABLE `gwarancje` (
  `id_gwarancje` int(10) UNSIGNED NOT NULL,
  `czas_gwarancji` int(10) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `opis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `gwarancje`
--

INSERT INTO `gwarancje` (`id_gwarancje`, `czas_gwarancji`, `nazwa`, `opis`) VALUES
(1, 24, '2 lata', 'Dwuletnia gwarancja'),
(2, 12, '1 rok', 'Gwarancja roczna');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategorie`
--

CREATE TABLE `kategorie` (
  `id_kategorie` int(10) UNSIGNED NOT NULL,
  `nazwa_kategorii` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kategorie`
--

INSERT INTO `kategorie` (`id_kategorie`, `nazwa_kategorii`) VALUES
(1, 'Koszulki'),
(2, 'Kurtki'),
(3, 'Buty');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `marki`
--

CREATE TABLE `marki` (
  `id_marki` int(10) UNSIGNED NOT NULL,
  `nazwa_marki` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `marki`
--

INSERT INTO `marki` (`id_marki`, `nazwa_marki`) VALUES
(1, 'Tommy Hilfiger'),
(2, 'Calvin Klein'),
(3, 'Ralph Lauren'),
(4, 'Vans'),
(5, 'Lacoste');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `opinie`
--

CREATE TABLE `opinie` (
  `id_opinie` int(10) UNSIGNED NOT NULL,
  `produkty_id_produktu` int(10) UNSIGNED NOT NULL,
  `tresc` varchar(255) NOT NULL,
  `data_wystawienia` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `platnosc`
--

CREATE TABLE `platnosc` (
  `id_platnosc` int(10) UNSIGNED NOT NULL,
  `rodzaj_platnosci` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `platnosc`
--

INSERT INTO `platnosc` (`id_platnosc`, `rodzaj_platnosci`) VALUES
(1, 'gotówka'),
(2, 'karta płatnicza'),
(3, 'przelew bankowy');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy`
--

CREATE TABLE `pracownicy` (
  `id_pracownicy` int(10) UNSIGNED NOT NULL,
  `imie` varchar(30) NOT NULL,
  `nazwisko` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `haslo` varchar(30) NOT NULL,
  `wynagrodzenie` float DEFAULT NULL,
  `stanowisko` varchar(30) NOT NULL,
  `data_zatrudnienia` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `pracownicy`
--

INSERT INTO `pracownicy` (`id_pracownicy`, `imie`, `nazwisko`, `email`, `haslo`, `wynagrodzenie`, `stanowisko`, `data_zatrudnienia`) VALUES
(1, 'Adam', 'Kowalski', 'adam.kowalski2019@gmail.com', 'haslo123', 2900, 'Manager', '2019-06-17'),
(3, 'Alicja', 'Kozik', 'alicja.kozik00@gmail.com', 'Haslo123', NULL, 'Właściciel', NULL),
(4, 'Wiktoria', 'Jancewicz', 'jancewicz0110@gmail.com', 'Haslo123', NULL, 'Właściciel', NULL),
(5, 'Paulina', 'Nowak', 'paulinanowak64@gmail.com', 'haslo123', 2000, 'Sprzedawca', '2019-07-23'),
(6, 'Marek', 'Szparek', 'marek.szparek.5@gmail.com', 'haslo123', 2500, 'Przedstawiciel handlowy', '2019-03-18');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkty`
--

CREATE TABLE `produkty` (
  `id_produktu` int(10) UNSIGNED NOT NULL,
  `gwarancje_id_gwarancje` int(10) UNSIGNED NOT NULL,
  `kategorie_id_kategorie` int(10) UNSIGNED NOT NULL,
  `marki_id_marki` int(10) UNSIGNED NOT NULL,
  `nazwa` varchar(200) NOT NULL,
  `cena_brutto` float NOT NULL,
  `cena_netto` float NOT NULL,
  `ilosc` int(10) UNSIGNED NOT NULL,
  `zdjecie` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `produkty`
--

INSERT INTO `produkty` (`id_produktu`, `gwarancje_id_gwarancje`, `kategorie_id_kategorie`, `marki_id_marki`, `nazwa`, `cena_brutto`, `cena_netto`, `ilosc`, `zdjecie`) VALUES
(3, 2, 1, 2, 'SMALL LOGO CLASSIC', 179, 145.53, 100, 'ck1.png'),
(4, 2, 1, 2, 'CORE MONOGRAM LOGO', 169, 137.4, 140, 'ck2.png'),
(5, 2, 1, 2, 'LOGO MODERN STRAIGHT TEE', 229, 186.18, 120, 'ck3.png'),
(6, 2, 1, 5, 'v-neck basic (navy)', 179, 145.53, 80, 'l1.png'),
(9, 2, 1, 5, 'FEMME - koszulka polo (calypso)', 379, 308.13, 10, 'l2.png'),
(10, 2, 1, 5, 'Bluzka z długim rękawem (gray)', 229, 186.18, 78, 'l3.png'),
(11, 2, 1, 1, 'SOFT V NECK TEE', 139, 113.01, 30, 'th1.png'),
(12, 2, 1, 1, 'TJW TOMMY BADGE TEE', 179, 145.53, 40, 'th2.png'),
(13, 2, 1, 1, 'EMBROIDERY GRAPHIC TEE', 179, 145.53, 89, 'th3.png'),
(14, 2, 1, 3, 'POLO BEAR by Ralph Lauren', 349, 283.74, 103, 'rl1.png'),
(15, 2, 1, 3, 'Basic -  hunt club green', 249, 202.44, 87, 'rl2.png'),
(16, 2, 1, 3, 'T-Shirt z nadriukiem - white', 289, 234.96, 67, 'rl3.png'),
(21, 1, 2, 1, 'BLOCK INSULATION JACKET', 859, 698.37, 106, 'k_th.png'),
(22, 1, 2, 2, 'OVERSIZED LOGO PUFFER', 1199, 974.8, 40, 'k_ck.png'),
(23, 1, 2, 5, 'Kurtka puchowa - eclipse blue/green', 1189, 966.67, 123, 'k_l.png'),
(24, 1, 2, 3, 'CIRE kurtka puchowa', 1229, 999.19, 87, 'k_rl.png'),
(25, 1, 3, 4, 'UA OLD SKOOL - niskie', 339, 275.61, 51, 'vans1.png'),
(26, 1, 3, 4, 'COMFYCUSH ERA', 339, 275.61, 11, 'vans2.png'),
(28, 1, 3, 4, 'CLASSIC - slippers', 279, 226.83, 134, 'vans3.png');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkty_has_zamowienia`
--

CREATE TABLE `produkty_has_zamowienia` (
  `produkty_id_produktu` int(10) UNSIGNED NOT NULL,
  `zamowienia_id_zamowienia` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `reklamacje`
--

CREATE TABLE `reklamacje` (
  `id_reklamacje` int(10) UNSIGNED NOT NULL,
  `zamowienia_id_zamowienia` int(10) UNSIGNED NOT NULL,
  `produkty_id_produktu` int(10) UNSIGNED NOT NULL,
  `opis` varchar(255) NOT NULL,
  `data_przyjecia` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

CREATE TABLE `uzytkownicy` (
  `id_uzytkownicy` int(10) UNSIGNED NOT NULL,
  `imie` varchar(50) NOT NULL,
  `nazwisko` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `haslo` varchar(50) NOT NULL,
  `data_urodzenia` date NOT NULL,
  `plec` varchar(30) NOT NULL,
  `miejscowosc` varchar(255) DEFAULT NULL,
  `wojewodztwo` varchar(255) DEFAULT NULL,
  `kod_pocztowy` varchar(6) DEFAULT NULL,
  `ulica` varchar(255) DEFAULT NULL,
  `nr_domu` int(10) UNSIGNED DEFAULT NULL,
  `nr_lokalu` int(10) UNSIGNED DEFAULT NULL,
  `login` varchar(100) NOT NULL,
  `administrator` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id_uzytkownicy`, `imie`, `nazwisko`, `email`, `haslo`, `data_urodzenia`, `plec`, `miejscowosc`, `wojewodztwo`, `kod_pocztowy`, `ulica`, `nr_domu`, `nr_lokalu`, `login`, `administrator`) VALUES
(3, 'Alicja', 'Kozik', 'alicja.kozik00@gmail.com', '1a7fcdd5a9fd433523268883cfded9d0', '2000-09-19', 'female', 'Racibórz', 'Śląskie', '47-400', 'Słowackiego', 2, 1, 'koziiczkaa', 1),
(9, 'Andrzej', 'Chmiel', 'andrzejchmiel@gmail.com', '1a7fcdd5a9fd433523268883cfded9d0', '0000-00-00', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Andret', 0),
(10, 'Wiktoria', 'Jancewicz', 'wiktoriajancewicz@gmail.com', '1a7fcdd5a9fd433523268883cfded9d0', '2000-10-01', 'female', 'Bierawa', 'Opolskie', '47-240', 'Raciborska', 1, 0, 'wiki11', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienia`
--

CREATE TABLE `zamowienia` (
  `id_zamowienia` int(10) UNSIGNED NOT NULL,
  `platnosc_id_platnosc` int(10) UNSIGNED NOT NULL,
  `faktury_id_faktury` int(10) UNSIGNED NOT NULL,
  `dostawa_id_dostawa` int(10) UNSIGNED NOT NULL,
  `pracownicy_id_pracownicy` int(10) UNSIGNED NOT NULL,
  `uzytkownicy_id_uzytkownicy` int(10) UNSIGNED NOT NULL,
  `data_zlozenia` datetime NOT NULL,
  `data_przyjecia` datetime NOT NULL,
  `data_wplaty` datetime NOT NULL,
  `data_realizacji` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dostawa`
--
ALTER TABLE `dostawa`
  ADD PRIMARY KEY (`id_dostawa`);

--
-- Indeksy dla tabeli `faktury`
--
ALTER TABLE `faktury`
  ADD PRIMARY KEY (`id_faktury`);

--
-- Indeksy dla tabeli `gwarancje`
--
ALTER TABLE `gwarancje`
  ADD PRIMARY KEY (`id_gwarancje`);

--
-- Indeksy dla tabeli `kategorie`
--
ALTER TABLE `kategorie`
  ADD PRIMARY KEY (`id_kategorie`);

--
-- Indeksy dla tabeli `marki`
--
ALTER TABLE `marki`
  ADD PRIMARY KEY (`id_marki`);

--
-- Indeksy dla tabeli `opinie`
--
ALTER TABLE `opinie`
  ADD PRIMARY KEY (`id_opinie`),
  ADD KEY `opinie_FKIndex1` (`produkty_id_produktu`);

--
-- Indeksy dla tabeli `platnosc`
--
ALTER TABLE `platnosc`
  ADD PRIMARY KEY (`id_platnosc`);

--
-- Indeksy dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD PRIMARY KEY (`id_pracownicy`);

--
-- Indeksy dla tabeli `produkty`
--
ALTER TABLE `produkty`
  ADD PRIMARY KEY (`id_produktu`),
  ADD KEY `produkty_FKIndex1` (`marki_id_marki`),
  ADD KEY `produkty_FKIndex2` (`kategorie_id_kategorie`),
  ADD KEY `produkty_FKIndex3` (`gwarancje_id_gwarancje`);

--
-- Indeksy dla tabeli `produkty_has_zamowienia`
--
ALTER TABLE `produkty_has_zamowienia`
  ADD PRIMARY KEY (`produkty_id_produktu`,`zamowienia_id_zamowienia`),
  ADD KEY `produkty_has_zamowienia_FKIndex1` (`produkty_id_produktu`),
  ADD KEY `produkty_has_zamowienia_FKIndex2` (`zamowienia_id_zamowienia`);

--
-- Indeksy dla tabeli `reklamacje`
--
ALTER TABLE `reklamacje`
  ADD PRIMARY KEY (`id_reklamacje`),
  ADD KEY `reklamacje_FKIndex1` (`zamowienia_id_zamowienia`),
  ADD KEY `reklamacje_FKIndex2` (`produkty_id_produktu`);

--
-- Indeksy dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  ADD PRIMARY KEY (`id_uzytkownicy`);

--
-- Indeksy dla tabeli `zamowienia`
--
ALTER TABLE `zamowienia`
  ADD PRIMARY KEY (`id_zamowienia`),
  ADD KEY `zamowienia_FKIndex1` (`uzytkownicy_id_uzytkownicy`),
  ADD KEY `zamowienia_FKIndex2` (`pracownicy_id_pracownicy`),
  ADD KEY `zamowienia_FKIndex3` (`dostawa_id_dostawa`),
  ADD KEY `zamowienia_FKIndex4` (`platnosc_id_platnosc`),
  ADD KEY `zamowienia_FKIndex5` (`faktury_id_faktury`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawa`
--
ALTER TABLE `dostawa`
  MODIFY `id_dostawa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `faktury`
--
ALTER TABLE `faktury`
  MODIFY `id_faktury` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `gwarancje`
--
ALTER TABLE `gwarancje`
  MODIFY `id_gwarancje` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `kategorie`
--
ALTER TABLE `kategorie`
  MODIFY `id_kategorie` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `marki`
--
ALTER TABLE `marki`
  MODIFY `id_marki` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `opinie`
--
ALTER TABLE `opinie`
  MODIFY `id_opinie` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `platnosc`
--
ALTER TABLE `platnosc`
  MODIFY `id_platnosc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  MODIFY `id_pracownicy` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `produkty`
--
ALTER TABLE `produkty`
  MODIFY `id_produktu` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT dla tabeli `reklamacje`
--
ALTER TABLE `reklamacje`
  MODIFY `id_reklamacje` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  MODIFY `id_uzytkownicy` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `zamowienia`
--
ALTER TABLE `zamowienia`
  MODIFY `id_zamowienia` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `produkty`
--
ALTER TABLE `produkty`
  ADD CONSTRAINT `produkty_ibfk_1` FOREIGN KEY (`gwarancje_id_gwarancje`) REFERENCES `gwarancje` (`id_gwarancje`),
  ADD CONSTRAINT `produkty_ibfk_2` FOREIGN KEY (`kategorie_id_kategorie`) REFERENCES `kategorie` (`id_kategorie`),
  ADD CONSTRAINT `produkty_ibfk_3` FOREIGN KEY (`marki_id_marki`) REFERENCES `marki` (`id_marki`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
