<!doctype html>
<html lang="pl">
	<head>
		<title>Fashion A&W </title>
		<meta charset="UTF-8"/>
		<meta name="keywords" content="fashion, moda, odzież, obuwie, buty, clothes, shoes, koszulki, kurtki, tshirts, jackets"/>
		<meta name="subject" content="html">
		<meta name="language" content="PL">
		<meta name="author" content="Kozik Alicja, alicja.kozik00@gmail.com">
		<meta name="description" content="This site is about HTML. Contains formules, tables and lists."/>

		<!--FONTS-->
		<link href="https://fonts.googleapis.com/css?family=Nunito:300,300i,400,400i,700,700i&display=swap" rel="stylesheet">
		
		<!--BOOTSTRAP CSS-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		<!--MAIN CSS-->
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
    <body>
		<nav>
			<ul class="navigation">
				<li class="nav logo">
					<a href="index.php"><img class="logo" src="images/logo.png"></a>
				</li>
				<li class="nav hover hvr-fade"><a class="link" href="odziez.php">ODZIEŻ</a></li>
				<li class="nav hover hvr-fade active"><a class="link active" href="obuwie.php">OBUWIE</a></li>
				<li class="nav hover hvr-fade"><a class="link" href="kontakt.php">KONTAKT</a></li>
				<li class="nav panel rej">
							
				<?php if (isset($_COOKIE['username'])) { ?>
					<img class="panel" src="images/account.png">
                    <a href="myaccount.php" class="panel myacc">MOJE KONTO</a>
				<?php } else { ?>
					<a class="nav" href="login.php">
						<img class="panel" src="images/authorize.png">
						<span class="panel">ZALOGUJ SIĘ</span>
					</a>
				<?php } ?>
				</li>
					
				<li class="nav panel szukaj">
					<img class="panel" src="images/search.png">
					<span class="panel find">SZUKAJ</span>
				</li>
			</ul>
		</nav>
		<br>
        <br>
        <span class="visibility" onClick="toggle_visibility('sort')">Otwórz filtry</span>
        <br>
        <br>
        <div class="filtr" id="sort" style="display: none;">
            <form class="sort">
                <div class="marka">
                    <label>Marka</label>
                    <br>
                    <input type="checkbox" name="marka[]" value="1"> Tommy Hilfiger<br>
                    <input type="checkbox" name="marka[]" value="2"> Calvin Klein<br>
                    <input type="checkbox" name="marka[]" value="3"> Ralph Lauren<br>
                    <input type="checkbox" name="marka[]" value="4"> Vans<br>
                    <input type="checkbox" name="marka[]" value="5"> Lacoste
                    <br>
                    <br>

                </div>
                <div class="cena">
                    <label class="cena">Cena</label>
                    <br>
                    <input class="cena" type="number" name="cenamin"
                        <?php
                            if(isset($_GET["cenamin"])){
                                echo "value = " . $_GET["cenamin"];
                            }   
                        ?>
                    >
                    <br>
                    <br>

                    <input class="cena" type="number" name="cenamax"
                        <?php
                            if(isset($_GET["cenamax"])){
                                echo "value = " . $_GET["cenamax"];
                            }   
                        ?>
                    >
                    <br>
                    <br>

                </div>
				<br>
				<br>
				<br>
				<br>
				<br>
                 
                <input class="filtr-submit" type="submit" value="Filtruj">
            </form>
        </div>
                <br>
        
        		<div class="productslist">
        
            <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kozjan";

$polaczenie = mysqli_connect($servername, $username, $password, $dbname);

mysqli_set_charset($polaczenie, "utf8");
if(!$polaczenie){
	die("Połączenie nie powiodło się!");
}

$zapytanie="SELECT * FROM produkty where kategorie_id_kategorie = '3'";
$wynik=mysqli_query($polaczenie, $zapytanie);


while($rekord = mysqli_fetch_assoc($wynik)){
		echo "<div class='produkt'>
        <img src='images/" .$rekord["zdjecie"]."'>
        <p>" .$rekord["nazwa"]."</p>
        <span>" .$rekord["cena_brutto"]."zł</span>
        <img src='images/basket.png' class='basket'>
		</div>
		";
}


mysqli_close($polaczenie);
?>
            
        </div>

		<div class="message">
			<h5 class="companiesmsg">Wybierz firmę, której produkty chcesz wyświetlić</h5>
		</div>

		<div class="contentspage">
			<ul class="companies">
				<li class="element">
					<a href="produkty2.php?marka=1"><img class="company" src="images/thlogo.png" alt=""></a>
				</li>
				<li class="element">
					<a href="produkty2.php?marka=2"><img class="company" src="images/cklogo.png" alt=""></a>
				</li>
				<li class="element">
					<a href="produkty2.php?marka=3"><img class="company" src="images/rllogo.png" alt=""></a>
				</li>
				<li class="element">						
					<a href="produkty2.php?marka=5"><img class="company" src="images/lacostelogo.png" alt=""></a>
				</li>
				<li class="element">
					<a href="produkty2.php?marka=4"><img class="company" src="images/vanslogo.png" alt=""></a>
				</li>
			</ul>
            <div class="stopka">
                <ul class="sociallist">
                    <a href=""><li class="socialimg firstel"><img class="social" src="images/fb.png" alt="fb"></li></a>
                    <a href=""><li class="socialimg"><img class="social" src="images/ig.png" alt="ig"></li></a>
                    <a href=""><li class="socialimg"><img class="social" src="images/twitter.png" alt="twitter"></li></a>
                    <li class="socialtxt">Copyright &copy; by Alicja Kozik & Wiktoria Jancewicz</li>
                </ul>
            </div>
		</div>
        


		<!--SCRIPTS-->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

		<script>
		
			$(document).ready(function() {
				$("#logout").on("click", function(event){
					console.log("test");
					document.cookie = "username= ; expires= Thu, 01 Jan 1970 00:00:00 GMT; path=/";
					location.reload();
				});
			});

		</script>
        
		<script type="text/javascript">
            function toggle_visibility(id) {
            var e = document.getElementById('sort');
            if(e.style.display == 'none')
                e.style.display = 'block';
            else
                e.style.display = 'none';
            }
        </script>

	</body>
</html>

