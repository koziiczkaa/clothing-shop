<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kozjan";

$connection = mysqli_connect($servername, $username, $password, $dbname);

$currentUser = $_COOKIE['username'];
mysqli_set_charset($connection, "utf8");
$request = "select * from uzytkownicy where `id_uzytkownicy` = '$currentUser'";
$result = mysqli_query($connection, $request);

$user = mysqli_fetch_assoc($result);
?>
<!doctype html>
<html lang="pl">
<head>
  <title>Fashion A&W </title>
  <meta charset="UTF-8"/>
  <meta name="keywords"
        content="fashion, moda, odzież, obuwie, buty, clothes, shoes, koszulki, kurtki, tshirts, jackets"/>
  <meta name="subject" content="html">
  <meta name="language" content="PL">
  <meta name="author" content="Kozik Alicja, alicja.kozik00@gmail.com">
  <meta name="description" content="This site is about HTML. Contains formules, tables and lists."/>

  <!--FONTS-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,300i,400,400i,700,700i&display=swap"
        rel="stylesheet">

  <!--BOOTSTRAP CSS-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!--MAIN CSS-->
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<nav>
  <ul class="navigation">
    <li class="nav logo">
      <a href="index.php"><img class="logo" src="images/logo.png"></a>
    </li>
    <li class="nav hover hvr-fade"><a class="link" href="odziez.php">ODZIEŻ</a></li>
    <li class="nav hover hvr-fade"><a class="link" href="obuwie.php">OBUWIE</a></li>
    <li class="nav hover hvr-fade"><a class="link" href="kontakt.php">KONTAKT</a></li>
    <li class="nav panel rej">

        <?php if (isset($currentUser)) { ?>
          <img class="panel" src="images/account.png">
          <a href="myaccount.php" class="panel myacc">MOJE KONTO</a>
        <?php } else { ?>
          <a class="nav" href="login.php">
            <img class="panel" src="images/authorize.png">
            <span class="panel">ZALOGUJ SIĘ</span>
          </a>
        <?php } ?>
    </li>

    <li class="nav panel szukaj">
      <img class="panel" src="images/search.png">
      <span class="panel find">SZUKAJ</span>
    </li>
  </ul>
</nav>
<div class="acc-content">
  <div class="acc-menu">
    <ul class="acc-list">
      <li><a class="acc-a" href="myaccount.php"><h3>Profil użytkownika</h3></a></li>
      <li><a class="acc-a" href="dataupdate.php">Zmień dane</a></li>
      <li><a class="acc-a" href="changepassword.php">Zmień hasło</a></li>
      <?php if ($user['administrator']) { ?>
      <li><a class="acc-a" href="adminpanel.php" style="color: #C7433C; font-weight: bold;">Panel
          administratora</a></li>
        <?php } ?>
      <li><a class="acc-a" href="#"><h3>Zamówienia</h3></a></li>
      <li><a class="acc-a" href="#">Moje zamówienia</a></li>
      <li><a class="acc-a" href="#">Reklamacje</a></li>
      <li><h3 class="panel logout" id="logout" style="font-weight: bold;">WYLOGUJ</h3></li>
    </ul>
  </div>
  <div class="acc-main">
    <h4 class="accupdate-header">Zaktualizuj swoje dane osobowe</h4>
    <br>
    <div id="accupdate-form">
      <input class="signup" type="text" name="city" placeholder="Miasto..." id="city"
             value="<?php echo $user['miejscowosc']; ?>"><br>
      <br><input class="signup" type="text" name="district" placeholder="Województwo..." id="district"
                 value="<?php echo $user['wojewodztwo']; ?>"><br>
      <br><input class="signup" type="text" name="postal" placeholder="Kod-pocztowy..." id="postal"
                 value="<?php echo $user['kod_pocztowy']; ?>"><br>
      <br><input class="signup" type="text" name="street" placeholder="Ulica..." id="street"
                 value="<?php echo $user['ulica']; ?>"><br>
      <br><input class="signup" type="number" name="building" placeholder="Nr budynku..."
                 id="building" value="<?php echo (int)$user['nr_domu']; ?>"><br>
      <br><input class="signup" type="number" name="flat" placeholder="Nr lokalu..."
                 id="flat" value="<?php echo (($user['nr_lokalu'] == -1) ? '' : $user['nr_lokalu']); ?>"><br>
      <br><input type="date" name="bday" id="bdaydate"
                 value="<?php echo date('Y-m-d', strtotime($user['data_urodzenia'])); ?>"><br>
      <br>
      <p>Wybierz płeć</p>
      <input type="radio" name="gender" value="male"
             class="gender" <?php echo $user['plec'] == 'male' ? 'checked' : ''; ?>>Mężczyzna<br>
      <br><input type="radio" name="gender" value="female"
                 class="gender" <?php echo $user['plec'] == 'female' ? 'checked' : ''; ?>>Kobieta<br>
      <br><input type="radio" name="gender" value="other"
                 class="gender" <?php echo $user['plec'] == 'other' ? 'checked' : ''; ?>>Inne<br>
      <br><input class="signup" type="submit" value="Zaktualizuj" id="update">
    </div>
  </div>
</div>

<div class="stopka">
  <ul class="sociallist">
    <a href="">
      <li class="socialimg firstel"><img class="social" src="images/fb.png" alt="fb"></li>
    </a>
    <a href="">
      <li class="socialimg"><img class="social" src="images/ig.png" alt="ig"></li>
    </a>
    <a href="">
      <li class="socialimg"><img class="social" src="images/twitter.png" alt="twitter"></li>
    </a>
    <li class="socialtxt">Copyright &copy; by Alicja Kozik & Wiktoria Jancewicz</li>
  </ul>
</div>

<!--SCRIPTS-->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>

    $(document).ready(function () {
        $('#logout').on('click', function () {
            console.log('test');
            document.cookie = 'username= ; expires= Thu, 01 Jan 1970 00:00:00 GMT; path=/';
            location.reload();
            window.location.href = 'index.php';
        });
        $('#update').on('click', function () {
            $.ajax({
                type: 'POST',
                url: 'db.php',
                data: {
                    id: document.cookie.split('=')[1],
                    city: $('#city').val(),
                    district: $('#district').val(),
                    postal: $('#postal').val(),
                    street: $('#street').val(),
                    building: $('#building').val(),
                    flat: $('#flat').val() || -1,
                    gender: $('.gender:checked').val(),
                    birthsday: $("#bdaydate").val(),
                    cmd: 5
                },
                success: function (results) {
                    var object = JSON.parse(results);

                    if (object.result === 0) {
                        alert('Dane poprawnie zmienione');
                    } else {
                        alert('Wystąpił błąd');
                    }
                }
            });
        });
    });

</script>

</body>
</html>

