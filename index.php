<!doctype html>
<html lang="pl">
	<head>
		<title>Fashion A&W </title>
		<meta charset="UTF-8"/>
		<meta name="keywords" content="fashion, moda, odzież, obuwie, buty, clothes, shoes, koszulki, kurtki, tshirts, jackets"/>
		<meta name="subject" content="html">
		<meta name="language" content="PL">
		<meta name="author" content="Kozik Alicja, alicja.kozik00@gmail.com">
		<meta name="description" content="This site is about HTML. Contains formules, tables and lists."/>

		<!--FONTS-->
		<link href="https://fonts.googleapis.com/css?family=Nunito:300,300i,400,400i,700,700i&display=swap" rel="stylesheet">
		
		<!--BOOTSTRAP CSS-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		<!--MAIN CSS-->
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
    <body>
        <nav>
            <ul class="navigation">
                <li class="nav logo">
					<a href="index.php"><img class="logo" src="images/logo.png"></a>
				</li>
                <li class="nav hover hvr-fade"><a class="link" href="odziez.php">ODZIEŻ</a></li>
                <li class="nav hover hvr-fade"><a class="link" href="obuwie.php">OBUWIE</a></li>
				<li class="nav hover hvr-fade"><a class="link" href="kontakt.php">KONTAKT</a></li>
					<li class="nav panel rej">
						<?php if (isset($_COOKIE['username'])) { ?>
							<img class="panel" src="images/account.png">
							<a href="myaccount.php" class="panel myacc">MOJE KONTO</a>
						<?php } else { ?>
							<a class="nav" href="login.php">
								<img class="panel" src="images/authorize.png">
								<span class="panel">ZALOGUJ SIĘ</span>
							</a>
						<?php } ?>
					</li>
				
					<li class="nav panel szukaj">
						<img class="panel" src="images/search.png">
						<span class="panel find">SZUKAJ</span>
					</li>
            </ul>
		</nav>
		
		<div id="karuzela" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
				<div class="carousel-item active">
					<img src="images/woman1.png" alt="First Slide" class="d-block w-100">
					<div class="carousel-caption">
						<h3>Odzież światowych marek!</h3>
						<p>Najlepsze marki w jednym miejscu</p>
					</div>
				</div>
				<div class="carousel-item">
						<img src="images/woman2.png" alt="Second Slide" class="d-block w-100">
						<div class="carousel-caption">
							<h3>Dogodne ceny!</h3>
							<p>Najniższe ceny na rynku</p>
						</div>
					</div>
				<div class="carousel-item">
					<img src="images/woman3.png" alt="Third Slide" class="d-block w-100">
					<div class="carousel-caption">
						<h3>Produkty sprowadzane z USA!</h3>
						<p>Najlepszej jakości oryginalna odzież i obuwie</p>
					</div>
				</div>
				<div class="carousel-item">
					<img src="images/woman4.png" alt="Fourth Slide" class="d-block w-100">
					<div class="carousel-caption">
						<h3>Zapisz się do newslettera!</h3>
						<p>Dzięki temu jako pierwszy będziesz wiedział o nadchodzących promocjach</p>
					</div>
				</div>
				<a class="carousel-control-prev" href="#karuzela" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#karuzela" role="button" data-slide="next">
						<span class="carousel-control-next-icon"></span>
						<span class="sr-only">Next</span>
					</a>
			</div>
		</div>

		<br>
        <br>
            
		<div class="message">
			<h5 class="companiesmsg">Wybierz firmę, której produkty chcesz wyświetlić</h5>
		</div>

		<div class="contentspage">
			<ul class="companies">
				<li class="element">
					<a href="produkty2.php?marka=1"><img class="company" src="images/thlogo.png" alt=""></a>
				</li>
				<li class="element">
					<a href="produkty2.php?marka=2"><img class="company" src="images/cklogo.png" alt=""></a>
				</li>
				<li class="element">
					<a href="produkty2.php?marka=3"><img class="company" src="images/rllogo.png" alt=""></a>
				</li>
				<li class="element">						
					<a href="produkty2.php?marka=5"><img class="company" src="images/lacostelogo.png" alt=""></a>
				</li>
				<li class="element">
					<a href="produkty2.php?marka=4"><img class="company" src="images/vanslogo.png" alt=""></a>
				</li>
			</ul>
            <div class="stopka">
                <ul class="sociallist">
                    <a href=""><li class="socialimg firstel"><img class="social" src="images/fb.png" alt="fb"></li></a>
                    <a href=""><li class="socialimg"><img class="social" src="images/ig.png" alt="ig"></li></a>
                    <a href=""><li class="socialimg"><img class="social" src="images/twitter.png" alt="twitter"></li></a>
                    <li class="socialtxt">Copyright &copy; by Alicja Kozik & Wiktoria Jancewicz</li>
                </ul>
            </div>
		</div>
        
		<!--SCRIPTS-->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		
		<script>
		
			$(document).ready(function() {
				$("#logout").on("click", function(event){
					console.log("test");
					document.cookie = "username= ; expires= Thu, 01 Jan 1970 00:00:00 GMT; path=/";
					location.reload();
				});
			});

		</script>

	</body>
</html>

